package com.whut.zjq.httpdemo;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;




public class MainActivity extends ActionBarActivity {
    private static final String TAG="MainActivity";
    private TextView resultText;//返回http请求结果
    private Button httpGet;//发送http请求结果
    private static final int HTTPGETSUCCESS=1;//http请求成功
    private static final int HTTPGETFAIL=0;//http请求失败
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        httpGet=(Button)findViewById(R.id.btnGet);
        resultText=(TextView)findViewById(R.id.resultText);
        httpGet.setOnClickListener( new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            LoadData loadData=new LoadData();
                                            new Thread(loadData).start();
                                        }
                                    }

        );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private class LoadData implements   Runnable {

        @Override
        public void run() {
            Message msg = handler.obtainMessage();
            String  url="http://iwut.wutnews.net/news/news_list?page=1&psize=10";

            try{
                Log.d(TAG,"请求的url为："+url);
                String result=Net.HttpGet(url);
                Log.d(TAG,"请求的结果为："+result);
                msg.what=HTTPGETSUCCESS;
                msg.obj = result;
            }catch (Exception e){
                msg.what=HTTPGETFAIL;

                e.printStackTrace();
            }
            msg.sendToTarget();

        }
    }
    private Handler handler =new Handler(){
         @Override
         public void handleMessage(Message msg){
              switch (msg.what){
                  case HTTPGETSUCCESS:
                      resultText.setText((String)msg.obj);
                      break;
                  case HTTPGETFAIL:
                      resultText.setText("http请求出错！");
                      break;
                  default :
                      break;
              }
         }
    };
}
