package com.whut.zjq.httpdemo;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * 张家强
 */
public class Net {


    /****
     * http请求数据
     * *******/
    public static String HttpGet(String urlStr) throws  Exception{
        HttpGet get = new HttpGet(urlStr);
        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 8000);
        HttpResponse response = client.execute(get);

        if (response.getStatusLine().getStatusCode() == 200) {
            HttpEntity entity = response.getEntity();
            if (Thread.interrupted())
                throw new IOException("Thread interrupted");
            String info = EntityUtils.toString(entity, "utf-8");
            return info;
        } else {
            throw new IOException("http connection failed!");
        }
    }

}
